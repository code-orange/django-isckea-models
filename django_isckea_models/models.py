# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Dhcp4Audit(models.Model):
    id = models.BigAutoField(primary_key=True)
    object_type = models.CharField(max_length=256)
    object_id = models.BigIntegerField()
    modification_type = models.ForeignKey(
        "Modification", models.DO_NOTHING, db_column="modification_type"
    )
    revision = models.ForeignKey("Dhcp4AuditRevision", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "dhcp4_audit"


class Dhcp4AuditRevision(models.Model):
    id = models.BigAutoField(primary_key=True)
    modification_ts = models.DateTimeField()
    log_message = models.TextField(blank=True, null=True)
    server_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp4_audit_revision"


class Dhcp4GlobalParameter(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=128)
    value = models.TextField()
    modification_ts = models.DateTimeField()
    parameter_type = models.ForeignKey(
        "ParameterDataType", models.DO_NOTHING, db_column="parameter_type"
    )

    class Meta:
        managed = False
        db_table = "dhcp4_global_parameter"


class Dhcp4GlobalParameterServer(models.Model):
    parameter = models.OneToOneField(
        Dhcp4GlobalParameter, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey("Dhcp4Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_global_parameter_server"
        unique_together = (("parameter", "server"),)


class Dhcp4OptionDef(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=128)
    space = models.CharField(max_length=128)
    type = models.PositiveIntegerField()
    modification_ts = models.DateTimeField()
    is_array = models.IntegerField()
    encapsulate = models.CharField(max_length=128)
    record_types = models.CharField(max_length=512, blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp4_option_def"


class Dhcp4OptionDefServer(models.Model):
    option_def = models.OneToOneField(
        Dhcp4OptionDef, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey("Dhcp4Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_option_def_server"
        unique_together = (("option_def", "server"),)


class Dhcp4Options(models.Model):
    option_id = models.BigAutoField(primary_key=True)
    code = models.PositiveIntegerField()
    value = models.TextField(blank=True, null=True)
    formatted_value = models.TextField(blank=True, null=True)
    space = models.CharField(max_length=128, blank=True, null=True)
    persistent = models.IntegerField()
    dhcp_client_class = models.CharField(max_length=128, blank=True, null=True)
    dhcp4_subnet_id = models.PositiveIntegerField(blank=True, null=True)
    host = models.ForeignKey("Hosts", models.DO_NOTHING, blank=True, null=True)
    scope = models.ForeignKey("DhcpOptionScope", models.DO_NOTHING)
    user_context = models.TextField(blank=True, null=True)
    shared_network_name = models.CharField(max_length=128, blank=True, null=True)
    pool_id = models.BigIntegerField(blank=True, null=True)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_options"


class Dhcp4OptionsServer(models.Model):
    option = models.OneToOneField(Dhcp4Options, models.DO_NOTHING, primary_key=True)
    server = models.ForeignKey("Dhcp4Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_options_server"
        unique_together = (("option", "server"),)


class Dhcp4Pool(models.Model):
    id = models.BigAutoField(primary_key=True)
    start_address = models.PositiveIntegerField()
    end_address = models.PositiveIntegerField()
    subnet = models.ForeignKey("Dhcp4Subnet", models.DO_NOTHING)
    modification_ts = models.DateTimeField()
    client_class = models.CharField(max_length=128, blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp4_pool"


class Dhcp4Server(models.Model):
    id = models.BigAutoField(primary_key=True)
    tag = models.CharField(unique=True, max_length=256)
    description = models.TextField(blank=True, null=True)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_server"


class Dhcp4SharedNetwork(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=128)
    client_class = models.CharField(max_length=128, blank=True, null=True)
    interface = models.CharField(max_length=128, blank=True, null=True)
    match_client_id = models.IntegerField(blank=True, null=True)
    modification_ts = models.DateTimeField()
    rebind_timer = models.IntegerField(blank=True, null=True)
    relay = models.TextField(blank=True, null=True)
    renew_timer = models.IntegerField(blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    reservation_mode = models.IntegerField(blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)
    valid_lifetime = models.IntegerField(blank=True, null=True)
    authoritative = models.IntegerField(blank=True, null=True)
    calculate_tee_times = models.IntegerField(blank=True, null=True)
    t1_percent = models.FloatField(blank=True, null=True)
    t2_percent = models.FloatField(blank=True, null=True)
    boot_file_name = models.CharField(max_length=512, blank=True, null=True)
    next_server = models.PositiveIntegerField(blank=True, null=True)
    server_hostname = models.CharField(max_length=512, blank=True, null=True)
    min_valid_lifetime = models.IntegerField(blank=True, null=True)
    max_valid_lifetime = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp4_shared_network"


class Dhcp4SharedNetworkServer(models.Model):
    shared_network = models.OneToOneField(
        Dhcp4SharedNetwork, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey(Dhcp4Server, models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_shared_network_server"
        unique_together = (("shared_network", "server"),)


class Dhcp4Subnet(models.Model):
    subnet_id = models.PositiveIntegerField(primary_key=True)
    subnet_prefix = models.CharField(unique=True, max_length=32)
    number_4o6_interface = models.CharField(
        db_column="4o6_interface", max_length=128, blank=True, null=True
    )  # Field renamed because it wasn't a valid Python identifier.
    number_4o6_interface_id = models.CharField(
        db_column="4o6_interface_id", max_length=128, blank=True, null=True
    )  # Field renamed because it wasn't a valid Python identifier.
    number_4o6_subnet = models.CharField(
        db_column="4o6_subnet", max_length=64, blank=True, null=True
    )  # Field renamed because it wasn't a valid Python identifier.
    boot_file_name = models.CharField(max_length=512, blank=True, null=True)
    client_class = models.CharField(max_length=128, blank=True, null=True)
    interface = models.CharField(max_length=128, blank=True, null=True)
    match_client_id = models.IntegerField(blank=True, null=True)
    modification_ts = models.DateTimeField()
    next_server = models.PositiveIntegerField(blank=True, null=True)
    rebind_timer = models.IntegerField(blank=True, null=True)
    relay = models.TextField(blank=True, null=True)
    renew_timer = models.IntegerField(blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    reservation_mode = models.IntegerField(blank=True, null=True)
    server_hostname = models.CharField(max_length=512, blank=True, null=True)
    shared_network_name = models.ForeignKey(
        Dhcp4SharedNetwork,
        models.DO_NOTHING,
        db_column="shared_network_name",
        blank=True,
        null=True,
    )
    user_context = models.TextField(blank=True, null=True)
    valid_lifetime = models.IntegerField(blank=True, null=True)
    authoritative = models.IntegerField(blank=True, null=True)
    calculate_tee_times = models.IntegerField(blank=True, null=True)
    t1_percent = models.FloatField(blank=True, null=True)
    t2_percent = models.FloatField(blank=True, null=True)
    min_valid_lifetime = models.IntegerField(blank=True, null=True)
    max_valid_lifetime = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp4_subnet"


class Dhcp4SubnetServer(models.Model):
    subnet = models.OneToOneField(Dhcp4Subnet, models.DO_NOTHING, primary_key=True)
    server = models.ForeignKey(Dhcp4Server, models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp4_subnet_server"
        unique_together = (("subnet", "server"),)


class Dhcp6Audit(models.Model):
    id = models.BigAutoField(primary_key=True)
    object_type = models.CharField(max_length=256)
    object_id = models.BigIntegerField()
    modification_type = models.ForeignKey(
        "Modification", models.DO_NOTHING, db_column="modification_type"
    )
    revision = models.ForeignKey("Dhcp6AuditRevision", models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "dhcp6_audit"


class Dhcp6AuditRevision(models.Model):
    id = models.BigAutoField(primary_key=True)
    modification_ts = models.DateTimeField()
    log_message = models.TextField(blank=True, null=True)
    server_id = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_audit_revision"


class Dhcp6GlobalParameter(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=128)
    value = models.TextField()
    modification_ts = models.DateTimeField()
    parameter_type = models.ForeignKey(
        "ParameterDataType", models.DO_NOTHING, db_column="parameter_type"
    )

    class Meta:
        managed = False
        db_table = "dhcp6_global_parameter"


class Dhcp6GlobalParameterServer(models.Model):
    parameter = models.OneToOneField(
        Dhcp6GlobalParameter, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey("Dhcp6Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_global_parameter_server"
        unique_together = (("parameter", "server"),)


class Dhcp6OptionDef(models.Model):
    id = models.BigAutoField(primary_key=True)
    code = models.PositiveSmallIntegerField()
    name = models.CharField(max_length=128)
    space = models.CharField(max_length=128)
    type = models.PositiveIntegerField()
    modification_ts = models.DateTimeField()
    is_array = models.IntegerField()
    encapsulate = models.CharField(max_length=128)
    record_types = models.CharField(max_length=512, blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_option_def"


class Dhcp6OptionDefServer(models.Model):
    option_def = models.OneToOneField(
        Dhcp6OptionDef, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey("Dhcp6Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_option_def_server"
        unique_together = (("option_def", "server"),)


class Dhcp6Options(models.Model):
    option_id = models.BigAutoField(primary_key=True)
    code = models.PositiveSmallIntegerField()
    value = models.TextField(blank=True, null=True)
    formatted_value = models.TextField(blank=True, null=True)
    space = models.CharField(max_length=128, blank=True, null=True)
    persistent = models.IntegerField()
    dhcp_client_class = models.CharField(max_length=128, blank=True, null=True)
    dhcp6_subnet_id = models.PositiveIntegerField(blank=True, null=True)
    host = models.ForeignKey("Hosts", models.DO_NOTHING, blank=True, null=True)
    scope = models.ForeignKey("DhcpOptionScope", models.DO_NOTHING)
    user_context = models.TextField(blank=True, null=True)
    shared_network_name = models.CharField(max_length=128, blank=True, null=True)
    pool_id = models.BigIntegerField(blank=True, null=True)
    pd_pool_id = models.BigIntegerField(blank=True, null=True)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_options"


class Dhcp6OptionsServer(models.Model):
    option = models.OneToOneField(Dhcp6Options, models.DO_NOTHING, primary_key=True)
    server = models.ForeignKey("Dhcp6Server", models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_options_server"
        unique_together = (("option", "server"),)


class Dhcp6PdPool(models.Model):
    id = models.BigAutoField(primary_key=True)
    prefix = models.CharField(max_length=45)
    prefix_length = models.IntegerField()
    delegated_prefix_length = models.IntegerField()
    subnet = models.ForeignKey("Dhcp6Subnet", models.DO_NOTHING)
    modification_ts = models.DateTimeField()
    excluded_prefix = models.CharField(max_length=45, blank=True, null=True)
    excluded_prefix_length = models.IntegerField()
    client_class = models.CharField(max_length=128, blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_pd_pool"


class Dhcp6Pool(models.Model):
    id = models.BigAutoField(primary_key=True)
    start_address = models.CharField(max_length=45)
    end_address = models.CharField(max_length=45)
    subnet = models.ForeignKey("Dhcp6Subnet", models.DO_NOTHING)
    modification_ts = models.DateTimeField()
    client_class = models.CharField(max_length=128, blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_pool"


class Dhcp6Server(models.Model):
    id = models.BigAutoField(primary_key=True)
    tag = models.CharField(unique=True, max_length=256)
    description = models.TextField(blank=True, null=True)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_server"


class Dhcp6SharedNetwork(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=128)
    client_class = models.CharField(max_length=128, blank=True, null=True)
    interface = models.CharField(max_length=128, blank=True, null=True)
    modification_ts = models.DateTimeField()
    preferred_lifetime = models.IntegerField(blank=True, null=True)
    rapid_commit = models.IntegerField(blank=True, null=True)
    rebind_timer = models.IntegerField(blank=True, null=True)
    relay = models.TextField(blank=True, null=True)
    renew_timer = models.IntegerField(blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    reservation_mode = models.IntegerField(blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)
    valid_lifetime = models.IntegerField(blank=True, null=True)
    calculate_tee_times = models.IntegerField(blank=True, null=True)
    t1_percent = models.FloatField(blank=True, null=True)
    t2_percent = models.FloatField(blank=True, null=True)
    interface_id = models.CharField(max_length=128, blank=True, null=True)
    min_preferred_lifetime = models.IntegerField(blank=True, null=True)
    max_preferred_lifetime = models.IntegerField(blank=True, null=True)
    min_valid_lifetime = models.IntegerField(blank=True, null=True)
    max_valid_lifetime = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_shared_network"


class Dhcp6SharedNetworkServer(models.Model):
    shared_network = models.OneToOneField(
        Dhcp6SharedNetwork, models.DO_NOTHING, primary_key=True
    )
    server = models.ForeignKey(Dhcp6Server, models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_shared_network_server"
        unique_together = (("shared_network", "server"),)


class Dhcp6Subnet(models.Model):
    subnet_id = models.PositiveIntegerField(primary_key=True)
    subnet_prefix = models.CharField(unique=True, max_length=64)
    client_class = models.CharField(max_length=128, blank=True, null=True)
    interface = models.CharField(max_length=128, blank=True, null=True)
    modification_ts = models.DateTimeField()
    preferred_lifetime = models.IntegerField(blank=True, null=True)
    rapid_commit = models.IntegerField(blank=True, null=True)
    rebind_timer = models.IntegerField(blank=True, null=True)
    relay = models.TextField(blank=True, null=True)
    renew_timer = models.IntegerField(blank=True, null=True)
    require_client_classes = models.TextField(blank=True, null=True)
    reservation_mode = models.IntegerField(blank=True, null=True)
    shared_network_name = models.ForeignKey(
        Dhcp6SharedNetwork,
        models.DO_NOTHING,
        db_column="shared_network_name",
        blank=True,
        null=True,
    )
    user_context = models.TextField(blank=True, null=True)
    valid_lifetime = models.IntegerField(blank=True, null=True)
    calculate_tee_times = models.IntegerField(blank=True, null=True)
    t1_percent = models.FloatField(blank=True, null=True)
    t2_percent = models.FloatField(blank=True, null=True)
    interface_id = models.CharField(max_length=128, blank=True, null=True)
    min_preferred_lifetime = models.IntegerField(blank=True, null=True)
    max_preferred_lifetime = models.IntegerField(blank=True, null=True)
    min_valid_lifetime = models.IntegerField(blank=True, null=True)
    max_valid_lifetime = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp6_subnet"


class Dhcp6SubnetServer(models.Model):
    subnet = models.OneToOneField(Dhcp6Subnet, models.DO_NOTHING, primary_key=True)
    server = models.ForeignKey(Dhcp6Server, models.DO_NOTHING)
    modification_ts = models.DateTimeField()

    class Meta:
        managed = False
        db_table = "dhcp6_subnet_server"
        unique_together = (("subnet", "server"),)


class DhcpOptionScope(models.Model):
    scope_id = models.PositiveIntegerField(primary_key=True)
    scope_name = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "dhcp_option_scope"


class HostIdentifierType(models.Model):
    type = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "host_identifier_type"


class Hosts(models.Model):
    host_id = models.AutoField(primary_key=True)
    dhcp_identifier = models.BinaryField(
        max_length=128
    )  # must be binary to store values
    dhcp_identifier_type = models.ForeignKey(
        HostIdentifierType, models.DO_NOTHING, db_column="dhcp_identifier_type"
    )
    dhcp4_subnet_id = models.PositiveIntegerField(blank=True, null=True)
    dhcp6_subnet_id = models.PositiveIntegerField(blank=True, null=True)
    ipv4_address = models.PositiveIntegerField(blank=True, null=True)
    hostname = models.CharField(max_length=255, blank=True, null=True)
    dhcp4_client_classes = models.CharField(max_length=255, blank=True, null=True)
    dhcp6_client_classes = models.CharField(max_length=255, blank=True, null=True)
    dhcp4_next_server = models.PositiveIntegerField(blank=True, null=True)
    dhcp4_server_hostname = models.CharField(max_length=64, blank=True, null=True)
    dhcp4_boot_file_name = models.CharField(max_length=128, blank=True, null=True)
    user_context = models.TextField(blank=True, null=True)
    auth_key = models.CharField(max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "hosts"
        unique_together = (
            ("ipv4_address", "dhcp4_subnet_id"),
            ("dhcp_identifier", "dhcp_identifier_type", "dhcp6_subnet_id"),
            ("dhcp_identifier", "dhcp_identifier_type", "dhcp4_subnet_id"),
        )


class Ipv6Reservations(models.Model):
    reservation_id = models.AutoField(primary_key=True)
    address = models.CharField(max_length=39)
    prefix_len = models.PositiveIntegerField()
    type = models.PositiveIntegerField()
    dhcp6_iaid = models.PositiveIntegerField(blank=True, null=True)
    host = models.ForeignKey(Hosts, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = "ipv6_reservations"
        unique_together = (("address", "prefix_len"),)


class Lease4(models.Model):
    address = models.PositiveIntegerField(primary_key=True)
    hwaddr = models.CharField(max_length=20, blank=True, null=True)
    client_id = models.CharField(max_length=128, blank=True, null=True)
    valid_lifetime = models.PositiveIntegerField(blank=True, null=True)
    expire = models.DateTimeField()
    subnet_id = models.PositiveIntegerField(blank=True, null=True)
    fqdn_fwd = models.IntegerField(blank=True, null=True)
    fqdn_rev = models.IntegerField(blank=True, null=True)
    hostname = models.CharField(max_length=255, blank=True, null=True)
    state = models.ForeignKey(
        "LeaseState", models.DO_NOTHING, db_column="state", blank=True, null=True
    )
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease4"


class Lease4Stat(models.Model):
    subnet_id = models.PositiveIntegerField(primary_key=True)
    state = models.PositiveIntegerField()
    leases = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease4_stat"
        unique_together = (("subnet_id", "state"),)


class Lease6(models.Model):
    address = models.CharField(primary_key=True, max_length=39)
    duid = models.CharField(max_length=128, blank=True, null=True)
    valid_lifetime = models.PositiveIntegerField(blank=True, null=True)
    expire = models.DateTimeField()
    subnet_id = models.PositiveIntegerField(blank=True, null=True)
    pref_lifetime = models.PositiveIntegerField(blank=True, null=True)
    lease_type = models.ForeignKey(
        "Lease6Types", models.DO_NOTHING, db_column="lease_type", blank=True, null=True
    )
    iaid = models.PositiveIntegerField(blank=True, null=True)
    prefix_len = models.PositiveIntegerField(blank=True, null=True)
    fqdn_fwd = models.IntegerField(blank=True, null=True)
    fqdn_rev = models.IntegerField(blank=True, null=True)
    hostname = models.CharField(max_length=255, blank=True, null=True)
    hwaddr = models.CharField(max_length=20, blank=True, null=True)
    hwtype = models.PositiveSmallIntegerField(blank=True, null=True)
    hwaddr_source = models.ForeignKey(
        "LeaseHwaddrSource",
        models.DO_NOTHING,
        db_column="hwaddr_source",
        blank=True,
        null=True,
    )
    state = models.ForeignKey(
        "LeaseState", models.DO_NOTHING, db_column="state", blank=True, null=True
    )
    user_context = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease6"


class Lease6Stat(models.Model):
    subnet_id = models.PositiveIntegerField(primary_key=True)
    lease_type = models.PositiveIntegerField()
    state = models.PositiveIntegerField()
    leases = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease6_stat"
        unique_together = (("subnet_id", "lease_type", "state"),)


class Lease6Types(models.Model):
    lease_type = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease6_types"


class LeaseHwaddrSource(models.Model):
    hwaddr_source = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=40, blank=True, null=True)

    class Meta:
        managed = False
        db_table = "lease_hwaddr_source"


class LeaseState(models.Model):
    state = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=64)

    class Meta:
        managed = False
        db_table = "lease_state"


class Logs(models.Model):
    timestamp = models.DateTimeField()
    address = models.CharField(max_length=43, blank=True, null=True)
    log = models.TextField()

    class Meta:
        managed = False
        db_table = "logs"


class Modification(models.Model):
    id = models.IntegerField(primary_key=True)
    modification_type = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = "modification"


class ParameterDataType(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    name = models.CharField(max_length=32)

    class Meta:
        managed = False
        db_table = "parameter_data_type"


class SchemaVersion(models.Model):
    version = models.IntegerField(primary_key=True)
    minor = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = "schema_version"
